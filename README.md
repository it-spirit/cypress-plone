# cypress-plone

Cypress plugin for the Plone CMS.

## Install with npm

```shell
npm install -D cypress-plone
```

## Install with Yarn

```shell
yarn add cypress-plone --dev
```

Then include in your project's `cypress/support/index.js`

```js
require("cypress-plone");
```

## Use

After installation your `cy` object will have a `ploneLoginAsUser` and a `ploneLoginAsRole` command.

```js
describe("Show how to activate the add-on", () => {
  beforeEach(() => {
    cy.ploneLoginAsRole("Manager");
  });

  it("can be installed", () => {
    cy.visit("/prefs_install_products_form");
    //...
  });
});
```

```js
describe("Show how to activate the add-on", () => {
  beforeEach(() => {
    cy.ploneLoginAsUser("admin", "topsecret");
  });

  it("can be installed", () => {
    cy.visit("/prefs_install_products_form");
    //...
  });
});
```

You can customize the username and password combinations in your cypress config:

```json
{
  "baseUrl": "http://localhost:8080/Plone",
  "env": {
    "SITE_OWNER_NAME": "admin",
    "SITE_OWNER_PASSWORD": "admin"
  }
}
```

## License

This project is licensed under the terms of the [MIT license](/LICENSE.md).
