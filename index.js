/// <reference types="cypress" />

const roles = {
  Manager: {
    username: Cypress.env("SITE_OWNER_NAME") || "admin",
    password: Cypress.env("SITE_OWNER_PASSWORD") || "secret",
  },
  TestUser: {
    username: Cypress.env("TEST_USER_NAME") || "test-user",
    password: Cypress.env("TEST_USER_PASSWORD") || "secret",
  },
};

const ploneLoginAsUser = (username, password) => {
  Cypress.log({
    name: "ploneLoginAsUser",
    message: `${username} | ${password}`,
  });

  return cy.request({
    method: "POST",
    url: "/login",
    form: true,
    body: {
      __ac_name: username,
      __ac_password: password,
      "buttons.login": "1",
    },
  });
};

const ploneLoginAsRole = (roleName) => {
  Cypress.log({
    name: "ploneLoginAsRole",
    message: `${roleName}`,
  });

  const { username, password } = roles[roleName];
  return ploneLoginAsUser(username, password);
};

Cypress.Commands.add("ploneLoginAsUser", ploneLoginAsUser);
Cypress.Commands.add("ploneLoginAsRole", ploneLoginAsRole);
